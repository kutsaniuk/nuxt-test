export const state = () => ({
    selling: false,
})

export const mutations = {
    SET_SELLING: (state, payload) => {
        state.selling = payload
    }
}
