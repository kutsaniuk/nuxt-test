export const state = () => ({
    cards: [
        {
            title: 'Old Republic',
            rating: 4.5,
            buyers: '5,423.00',
            lenders: '1,192.00',
            price: '6,615.00'
        },
        {
            title: 'First American',
            rating: 3,
            buyers: '5,423.00',
            lenders: '1,192.00',
            price: '7,389.00'
        },
        {
            title: 'Deal Star',
            rating: 5,
            buyers: '4,423.00',
            lenders: '1,723.00',
            price: '5,751.00'
        },
        {
            title: 'Old Republic',
            rating: 4.5,
            buyers: '5,423.00',
            lenders: '1,192.00',
            price: '6,615.00'
        },
        {
            title: 'First American',
            rating: 3,
            buyers: '5,423.00',
            lenders: '1,192.00',
            price: '7,389.00'
        },
        {
            title: 'Deal Star',
            rating: 5,
            buyers: '4,423.00',
            lenders: '1,723.00',
            price: '5,751.00'
        },
        {
            title: 'Old Republic',
            rating: 4.5,
            buyers: '5,423.00',
            lenders: '1,192.00',
            price: '6,615.00'
        },
        {
            title: 'First American',
            rating: 3,
            buyers: '5,423.00',
            lenders: '1,192.00',
            price: '7,389.00'
        },
        {
            title: 'Deal Star',
            rating: 5,
            buyers: '4,423.00',
            lenders: '1,723.00',
            price: '5,751.00'
        },
        {
            title: 'Old Republic',
            rating: 4.5,
            buyers: '5,423.00',
            lenders: '1,192.00',
            price: '6,615.00'
        },
        {
            title: 'First American',
            rating: 3,
            buyers: '5,423.00',
            lenders: '1,192.00',
            price: '7,389.00'
        },
        {
            title: 'Deal Star',
            rating: 5,
            buyers: '4,423.00',
            lenders: '1,723.00',
            price: '5,751.00'
        }
    ]
})
